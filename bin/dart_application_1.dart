import 'dart:io' show stdin;

void main() {
  print(
      "Select the choice you want to parform \n1. ADD \n2. SUBTRACT\n3. MULTIPLY\n4. DIVIDE\n5. EXIT");
  print("Choice you want to enter : ");
  int? n = int.parse(stdin.readLineSync()!);
  switch (n) {
    case 1:
      {
        print("Enter the value for x : ");
        int? new1 = int.parse(stdin.readLineSync()!);
        print("Enter the value for y : ");
        int? new2 = int.parse(stdin.readLineSync()!);
        n = new1 + new2;
        print("sum of the two numbers is : ");
        print(n);
        break;
      }
    case 2:
      {
        print("Enter the value for x : ");
        int? new1 = int.parse(stdin.readLineSync()!);
        print("Enter the value for y : ");
        int? new2 = int.parse(stdin.readLineSync()!);
        n = new1 - new2;
        print("Diff of the two numbers is : ");
        print(n);
        break;
      }
    case 3:
      {
        print("Enter the value for x : ");
        int? new1 = int.parse(stdin.readLineSync()!);
        print("Enter the value for y : ");
        int? new2 = int.parse(stdin.readLineSync()!);
        n = new1 * new2;
        print("Mul of the two numbers is : ");
        print(n);
        break;
      }
    case 4:
      {
        print("Enter the value for x : ");
        int? new1 = int.parse(stdin.readLineSync()!);
        print("Enter the value for y : ");
        int? new2 = int.parse(stdin.readLineSync()!);
        n = new1 ~/ new2;
        print("Division of the two numbers is : ");
        print(n);
        break;
      }
    case 5:
      {
        break;
      }
  }
}
